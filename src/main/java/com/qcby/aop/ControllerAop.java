package com.qcby.aop;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ControllerAop {
    @Pointcut("execution(* com.qcby.controller.*.*(..))")
    public void addLog(){};

    @Before("addLog()")
    public void before() {
        System.out.println("好好學習");
    }
    @Around("addLog()")
    public Object  testAround(ProceedingJoinPoint joinPoint){
        Object[] args = joinPoint.getArgs();
        for (Object arg : args) {
            System.out.println(arg);
        }
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        //2.最关键的一步:通过这获取到方法的所有参数名称的字符串数组
        String[] parameterNames = methodSignature.getParameterNames();
        System.out.println(methodSignature.getMethod().getName());
        System.out.println(methodSignature.getName());
        for (String parameterName : parameterNames) {
            System.out.println(parameterName+"==");
        }
        Object target = joinPoint.getTarget();
        System.out.println(target);
        try {
            long l = System.currentTimeMillis();
            Object proceed = joinPoint.proceed();
            long l1 = System.currentTimeMillis();
            System.out.println(l1-l);
            return   proceed;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return  null;
    }
}
