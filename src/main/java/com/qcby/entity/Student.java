package com.qcby.entity;

public class Student {
    private Integer id;

    private String studentName;

    private String studentSex;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName == null ? null : studentName.trim();
    }

    public String getStudentSex() {
        return studentSex;
    }

    public void setStudentSex(String studentSex) {
        this.studentSex = studentSex == null ? null : studentSex.trim();
    }

	@Override
	public String toString() {
		return "Student [id=" + id + ", studentName=" + studentName + ", studentSex=" + studentSex + "]";
	}
    
}