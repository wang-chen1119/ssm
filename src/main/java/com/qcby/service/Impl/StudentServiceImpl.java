package com.qcby.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qcby.dao.StudentMapper;
import com.qcby.entity.Student;
import com.qcby.service.StudentService;

@Service("studentService")
public class StudentServiceImpl implements StudentService{

	@Autowired
	private StudentMapper studentMapper;
	@Override
	public int add(Student student) {
		
		return studentMapper.insert(student);
	}
	@Override
	public Student find(Integer id) {
		return studentMapper.selectByPrimaryKey(id);
	}
	@Override
	public List<Student> findListByStudent(Student student) {
		return studentMapper.findListByStudent(student);
	}

	
}
